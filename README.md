# Chapter 4: Securing an Application with Spring Boot

## Api testing

Add new video with the following request:
```shell
curl -v -X POST localhost:8081/api/videos -d '{"name": "Learning Spring Boot 3"}' -H 'Content-type:application/json'
```

Get the list of all videos:
```shell
curl localhost:8081/api/videos
```
