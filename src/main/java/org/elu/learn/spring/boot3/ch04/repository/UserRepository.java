package org.elu.learn.spring.boot3.ch04.repository;

import org.elu.learn.spring.boot3.ch04.model.UserAccount;
import org.springframework.data.repository.Repository;

public interface UserRepository extends Repository<UserAccount, Long> {
    UserAccount findByUsername(String username);
}
