package org.elu.learn.spring.boot3.ch04.service;

import jakarta.annotation.PostConstruct;
import org.elu.learn.spring.boot3.ch04.model.NewVideo;
import org.elu.learn.spring.boot3.ch04.model.Search;
import org.elu.learn.spring.boot3.ch04.model.VideoEntity;
import org.elu.learn.spring.boot3.ch04.repository.VideoRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class VideoService {
    private final VideoRepository videoRepository;

    public VideoService(VideoRepository videoRepository) {
        this.videoRepository = videoRepository;
    }

    public List<VideoEntity> getVideos() {
        return videoRepository.findAll();
    }

    public VideoEntity create(NewVideo newVideo, String username) {
        return videoRepository.saveAndFlush(new VideoEntity(username, newVideo.name(), newVideo.description()));
    }

    public List<VideoEntity> search(Search search) {
        VideoEntity probe = new VideoEntity();
        if (StringUtils.hasText(search.value())) {
            probe.setName(search.value());
            probe.setDescription(search.value());
        }
        Example<VideoEntity> example = Example.of(probe,
            ExampleMatcher.matchingAny()
                          .withIgnoreCase()
                          .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING));
        return videoRepository.findAll(example);
    }

    @PostConstruct
    void initDatabase() {
        videoRepository.save(
            new VideoEntity("alice",
                "Need HELP with your SPRING BOOT 3 App?",
                "SPRING BOOT 3 will only speed things up and make it super SIMPLE to serve templates and raw data."));
        videoRepository.save(
            new VideoEntity("alice",
                "Don't do THIS to your own CODE!",
                "As a pro developer, never ever EVER do this to your code. Because you'll ultimately be doing it to YOURSELF!"));
        videoRepository.save(
            new VideoEntity("bob",
                "SECRETS to fix BROKEN CODE!",
                "Discover ways to not only debug your code, but to regain your confidence and get back in the game as a software developer."));
    }

    public void delete(Long videoId) {
        videoRepository
            .findById(videoId)
            .map(videoEntity -> {
                videoRepository.delete(videoEntity);
                return true;
            })
            .orElseThrow(() -> new IllegalArgumentException("No video with ID %s".formatted(videoId)));
    }
}
