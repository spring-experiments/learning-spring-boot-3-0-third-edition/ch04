package org.elu.learn.spring.boot3.ch04.model;

public record NewVideo(String name, String description) {
}
