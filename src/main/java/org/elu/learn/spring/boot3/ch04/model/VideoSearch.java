package org.elu.learn.spring.boot3.ch04.model;

public record VideoSearch(String name, String description) {
}
