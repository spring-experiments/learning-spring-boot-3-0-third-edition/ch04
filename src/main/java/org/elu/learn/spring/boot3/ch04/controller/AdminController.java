package org.elu.learn.spring.boot3.ch04.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {
    @GetMapping("/admin")
    public String adminPage() {
        return "admin";
    }
}
