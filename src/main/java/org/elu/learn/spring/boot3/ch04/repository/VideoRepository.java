package org.elu.learn.spring.boot3.ch04.repository;

import org.elu.learn.spring.boot3.ch04.model.VideoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface VideoRepository extends JpaRepository<VideoEntity, Long> {
    List<VideoEntity> findByNameContainsOrDescriptionContainsAllIgnoreCase(String name, String description);

    List<VideoEntity> findByNameContainsIgnoreCase(String name);

    List<VideoEntity> findByDescriptionContainsIgnoreCase(String description);

    @PreAuthorize("#entity.username == authentication.name")
    @Override
    void delete(VideoEntity entity);
}
