package org.elu.learn.spring.boot3.ch04.repository;

import org.elu.learn.spring.boot3.ch04.model.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserManagementRepository extends JpaRepository<UserAccount, Long> {
}
